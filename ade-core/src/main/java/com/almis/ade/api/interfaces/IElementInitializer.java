package com.almis.ade.api.interfaces;

public interface IElementInitializer {
  /**
   * Initialize element
   */
  void initialize();
}
