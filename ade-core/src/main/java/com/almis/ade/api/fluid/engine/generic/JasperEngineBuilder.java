package com.almis.ade.api.fluid.engine.generic;

import com.almis.ade.api.bean.component.Element;
import com.almis.ade.api.bean.input.PrintBean;
import com.almis.ade.api.engine.jasper.generation.builder.JasperDocumentBuilder;
import com.almis.ade.api.engine.jasper.generation.builder.component.element.ElementBuilder;
import com.almis.ade.api.enumerate.Section;
import com.almis.ade.config.AdeConfigProperties;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;

import java.util.Map;

/**
 *
 * @author dfuentes
 */
public class JasperEngineBuilder {

  private final AdeConfigProperties adeConfigProperties;

  private final JasperDocumentBuilder jasperDocumentBuilder;

  /**
   * JasperEngineBuilder constructor
   * @param adeConfigProperties ADE config properties
   */
  public JasperEngineBuilder(AdeConfigProperties adeConfigProperties) {
    this.adeConfigProperties = adeConfigProperties;
    this.jasperDocumentBuilder = new JasperDocumentBuilder();
  }

  /**
   * Set section builder mapping
   *
   * @param builderMapper builder mapper
   * @return JasperEngineBuilderService
   */
  public JasperEngineBuilder setSectionBuilderMapping(Map<Section, ElementBuilder> builderMapper) {
   jasperDocumentBuilder
            .getBuilderMapper()
            .setSectionBuilders(builderMapper);
    return this;
  }

  /**
   * Set builder mapping
   *
   * @param builderMapper builder mapper
   * @return JasperEngineBuilderService
   */
  public JasperEngineBuilder setBuilderMapping(Map<Class<? extends Element>, ElementBuilder> builderMapper) {

    jasperDocumentBuilder
            .getBuilderMapper()
            .setBuilders(builderMapper);
    return this;
  }

  /**
   * Set section builder mapping
   *
   * @param section section
   * @param builder builder
   * @return JasperEngineBuilderService
   */
  public JasperEngineBuilder setSectionBuilderMapping(Section section, ElementBuilder builder) {
    jasperDocumentBuilder
            .getBuilderMapper()
            .addSectionBuilder(section, builder);
    return this;
  }

  /**
   * Set builder mapping
   *
   * @param bean bean
   * @param builder element builder
   * @return JasperEngineBuilderService
   */
  public JasperEngineBuilder setBuilderMapping(Class<? extends Element> bean, ElementBuilder builder) {
    jasperDocumentBuilder
            .getBuilderMapper()
            .addBuilder(bean, builder);
    return this;
  }

  /**
   * Build current report and export to files
   *
   * @param printBean print bean
   * @return TemplateExporterBuilderService
   */
  public TemplateExporterBuilder buildAndExport(PrintBean printBean) {

    //Get current report object
    JasperReportBuilder reportBuilder = jasperDocumentBuilder.build(printBean);

    return new TemplateExporterBuilder(adeConfigProperties).initialize(reportBuilder);
  }
}
