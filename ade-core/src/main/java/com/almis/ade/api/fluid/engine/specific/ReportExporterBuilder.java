package com.almis.ade.api.fluid.engine.specific;

import lombok.Data;
import lombok.experimental.Accessors;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.*;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

/**
 * ReportExporterBuilder class
 *
 * @author dfuentes
 */
@Data
@Accessors(chain = true)
public class ReportExporterBuilder {

  private static final String EXTENSION_PATTERN = "{extension}";
  private List<JasperPrint> reportList;
  private String defaultPath;
  private String defaultName;
  private String filePassword;

  /**
   * Initialize class
   *
   * @param reportList JasperPrint report list
   * @param path report path
   * @param name report name
   * @param password report password (for file encoding)
   * @return SpecificTemplateExporterBuilderService
   */
  public ReportExporterBuilder initialize(List<JasperPrint> reportList, String path, String name, String password) {
    this.reportList = reportList;
    this.defaultPath = path;
    this.defaultName = name;
    this.filePassword = password;
    return this;
  }

  /**
   * Get formatted template path
   *
   * @return template path
   */
  public String getTemplatePath() {
    this.defaultPath = defaultPath.endsWith(File.separator) ? defaultPath : defaultPath + File.separator;

    if (Paths.get(defaultPath).toFile().mkdirs()) {
      //Throw exception, couldn't create directories
    }

    return defaultPath + (defaultName != null ? defaultName + "." + EXTENSION_PATTERN : "");
  }

  /**
   * Export to PDF
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toPdf() throws JRException {
    JRPdfExporter exporter = new JRPdfExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "pdf"))));

    // Add password if defined
    if (filePassword != null) {
      SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
      configuration.setEncrypted(true);
      configuration.setUserPassword(filePassword);
      configuration.setOwnerPassword(filePassword);
      exporter.setConfiguration(configuration);
    }

    exporter.exportReport();
    return this;
  }

  /**
   * Export to PPTX
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toPptx() throws JRException {
    JRPptxExporter exporter = new JRPptxExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "pptx"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to XML
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toXml() throws JRException {
    JRXmlExporter exporter = new JRXmlExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleXmlExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "xml"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to HTML
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toHtml() throws JRException {
    HtmlExporter exporter = new HtmlExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleHtmlExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "html"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to CSV
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toCsv() throws JRException {
    JRCsvExporter exporter = new JRCsvExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleWriterExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "csv"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to DOCX
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toDocx() throws JRException {
    JRDocxExporter exporter = new JRDocxExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "docx"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to EXCEL
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toExcel() throws JRException {
    JRXlsxExporter exporter = new JRXlsxExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "xlsx"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to ODS
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toOds() throws JRException {
    JROdsExporter exporter = new JROdsExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "ods"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to RTF
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toRtf() throws JRException {
    JRRtfExporter exporter = new JRRtfExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleWriterExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "rtf"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to TXT
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toText() throws JRException {
    JRTextExporter exporter = new JRTextExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleWriterExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "txt"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to XLS
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toXls() throws JRException {
    JRXlsExporter exporter = new JRXlsExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "xls"))));
    exporter.exportReport();
    return this;
  }

  /**
   * Export to XLSX
   *
   * @return SpecificTemplateExporterBuilderService
   * @throws JRException JRException exception
   */
  public ReportExporterBuilder toXlsx() throws JRException {
    JRXlsxExporter exporter = new JRXlsxExporter();
    exporter.setExporterInput(SimpleExporterInput.getInstance(reportList));
    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(getTemplatePath().replace(EXTENSION_PATTERN, "xls"))));
    exporter.exportReport();
    return this;
  }
}