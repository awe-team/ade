package com.almis.ade.api;

import com.almis.ade.api.bean.input.ReportBean;
import com.almis.ade.api.fluid.PrintBeanBuilder;
import com.almis.ade.api.fluid.ReportBeanBuilder;
import com.almis.ade.config.AdeConfigProperties;

/**
 * ADE API Base File
 * @author dfuentes
 */
public class ADE {

  // Autowired services
  private AdeConfigProperties adeConfigProperties;

  /**
   * Autowired constructor
   *
   * @param adeConfigProperties AWE config propertiess
   */
  public ADE(AdeConfigProperties adeConfigProperties) {
    this.adeConfigProperties = adeConfigProperties;
  }

  /**
   * Print dynamically generated report
   *
   * @return PrintBeanBuilderService
   */
  public PrintBeanBuilder printBean() {
    return new PrintBeanBuilder(adeConfigProperties);
  }

  /**
   * Print report from given JRXML file
   *
   * @param reportBean specific report bean
   * @return ReportBeanBuilderService
   */
  public ReportBeanBuilder reportBean(ReportBean reportBean) {
    return new ReportBeanBuilder().initialize(reportBean);
  }
}
