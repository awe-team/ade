package com.almis.ade.api.fluid;

import com.almis.ade.api.bean.input.ReportBean;
import com.almis.ade.api.bean.input.TemplateBean;
import com.almis.ade.api.fluid.engine.specific.ReportExporterBuilder;
import com.almis.ade.exception.ADEException;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * SpecificPrintBeanBuilderService class
 *
 * @author dfuentes
 */
@Slf4j
@Data
@Accessors(chain = true)
public class ReportBeanBuilder {

  // Constants
  private static final CharSequence EXTENSION_JASPER = ".JASPER";
  private ReportExporterBuilder reportExporterBuilder;
  private ReportBean reportBean;

  /**
   * Initialize class
   *
   * @param reportBean Specific report bean
   * @return SpecificPrintBeanBuilderService
   */
  public ReportBeanBuilder initialize(ReportBean reportBean) {
    return setReportBean(reportBean);
  }

  /**
   * Build report and set data to export
   *
   * @return ReportExporterBuilderService
   */
  public ReportExporterBuilder build() {
    List<JasperPrint> reportList = reportBean
      .getReportList()
      .stream()
      .map(this::compileTemplate)
      .filter(Objects::nonNull)
      .collect(Collectors.toList());

    return new ReportExporterBuilder().initialize(
      reportList,
      reportBean.getReportPath(),
      reportBean.getReportName(),
      reportBean.getReportPassword()
    );
  }

  /**
   * Compile template with data
   *
   * @param templateBean Template bean
   * @return JasperPrint object
   */
  private JasperPrint compileTemplate(TemplateBean templateBean) throws ADEException {
    try {
      JasperReport report;
      final InputStream jasperTemplate = templateBean.getTemplate().getInputStream();

      if (alreadyCompiled(templateBean)) {
        report = (JasperReport) JRLoader.loadObject(templateBean.getTemplate().getURL());
      } else {
        // Compile
        report = JasperCompileManager.compileReport(jasperTemplate);
      }

      // Generate collection if defined
      JRDataSource data = new JREmptyDataSource();
      if (templateBean.getData() != null) {
        data = new JRBeanCollectionDataSource(templateBean.getData());
      }

      return JasperFillManager.fillReport(report, templateBean.getParameters(), data);
    } catch (Exception exc) {
      log.error("Jasper error generating report from template: {}", templateBean.getTemplateName(), exc);
      throw new ADEException("Error generating report from template: " + templateBean.getTemplateName(), exc);
    }
  }

  /**
   * Check if template is already compiled
   *
   * @param templateBean Jasper template bean
   * @return true if is compiled
   */
  private boolean alreadyCompiled(TemplateBean templateBean) {
    return Optional.ofNullable(templateBean.getTemplate().getFilename()).orElse("")
      .toUpperCase()
      .contains(EXTENSION_JASPER);
  }
}
