package com.almis.ade.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 *   ADE configuration properties
 */
@ConfigurationProperties(prefix = "ade")
@Data
public class AdeConfigProperties {

  @NestedConfigurationProperty
  private final Document document = new Document();

  /**
   * Document config.
   */
  @Data
  public static class Document {

    /**
     * Default report path
     */
    private String defaultPath = "documents";
    /**
     * Default report name
     */
    private String defaultName = "document";
  }

}
