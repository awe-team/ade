package com.almis.ade.exception;

public class ADEException extends RuntimeException {
  public ADEException(String message, Exception exc) {
    super(message, exc);
  }
}
